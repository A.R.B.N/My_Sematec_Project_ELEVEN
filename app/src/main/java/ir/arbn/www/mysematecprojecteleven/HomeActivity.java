package ir.arbn.www.mysematecprojecteleven;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class HomeActivity extends AppCompatActivity {
    ImageView myImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        myImg = findViewById(R.id.myImg);
        findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myImg.getVisibility() == view.INVISIBLE)
                    myImg.setVisibility(View.VISIBLE);
                else
                    myImg.setVisibility(View.INVISIBLE);
            }
        });
    }
}
